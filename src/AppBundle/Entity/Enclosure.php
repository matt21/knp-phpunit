<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 04.02.18
 * Time: 14:58
 */

namespace AppBundle\Entity;


use AppBundle\Exception\DinosaursAreRunningRampantException;
use AppBundle\Exception\NotABuffetException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="enclosure")
 */
class Enclosure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    public function __construct(bool $withBasicSecurity = false)
    {
        $this->securities = new ArrayCollection();
        $this->dinosaurs = new ArrayCollection();

        if ($withBasicSecurity){
           $this->addSecurity(new Security('Fence', true, $this));
        }
    }

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Dinosaur", mappedBy="enclosure", cascade={"persist"})
     */
    private $dinosaurs;

        public function addDinosaur(Dinosaur $dinosaur)
        {
            if (!$this->canAddDinosaur($dinosaur)){
                throw new NotABuffetException();
            }
            if (!$this->isSecurityActive()){
                throw new DinosaursAreRunningRampantException('Are You crazy?!?!');
            }
            $this->dinosaurs->add($dinosaur);
        }

        /**
         * @return Collection
         */
        public function getDinosaurs(): Collection
        {
            return $this->dinosaurs;
        }

    /**
     * @var Collection|Security[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security", mappedBy="enclosure", cascade={"persist"})
     */
    private $securities;

        public function addSecurity(Security $security)
        {
            $this->securities[] = $security;
        }

        public function getSecurities(): Collection
        {
            return $this->securities;
        }

    public function canAddDinosaur(Dinosaur $dinosaur): bool
    {
        return count($this->dinosaurs) === 0 || $this->dinosaurs->first()->isCarnivorous() === $dinosaur->isCarnivorous();
    }

    public function isSecurityActive(): bool
    {
        foreach ($this->securities as $security){
            if($security->getIsActive()){
                return true;
            }
        }
        return false;
    }
}