<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="dinosaurs")
 */
class Dinosaur
{
    const LARGE = 10;
    const HUGE = 30;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Dinosaur constructor.
     * @param string $genus
     * @param bool $isCarnivorous
     */
    public function __construct(string $genus = 'unknown', bool $isCarnivorous = false)
    {
        $this->genus = $genus;
        $this->isCarnivorous = $isCarnivorous;
    }

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Enclosure", inversedBy="dinosaurs")
     */
    private $enclosure;

    /**
     * @ORM\Column(type="integer")
     */
    private $length = 0;

        /**
         * @return mixed
         */
        public function getLength() : int
        {
            return $this->length;
        }

        /**
         * @param mixed $length
         */
        public function setLength(int $length)
        {
            $this->length = $length;
        }

    /**
     * @ORM\Column(type="string")
     */
    private $genus;

        /**
         * @return mixed
         */
        public function getGenus()
        {
            return $this->genus;
        }

    /**
     * @ORM\Column(type="boolean")
     */
    private $isCarnivorous;

        /**
         * @return bool
         */
        public function isCarnivorous()
        {
            return $this->isCarnivorous;
        }


    /**
     * @return string
     */
    public function getSpecification() : string
    {
        return sprintf(
            'The %s %scarnivorous dino is %d meters long',
            $this->genus,
            $this->isCarnivorous ? '' : 'non-',
            $this->getLength()
        );
    }
}
